﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PelotaLauncher : FireTemplate
{
    public float throughForce;

    public override void Fire()
    {
        GetComponent<Rigidbody>().AddForce(throughForce * transform.forward, ForceMode.Impulse);
    }

    public override void Fire2()
    {
        GetComponent<Rigidbody>().AddForce(throughForce * transform.forward, ForceMode.Impulse);
    }

    public void pelotaforce(float value)
    {
        value = throughForce;
    }
}
