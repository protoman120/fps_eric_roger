﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestShot : MonoBehaviour
{
    [SerializeField] private PelotaLauncher launcher;
    private float throughForce;
    // Update is called once per frame

    private void Start()
    {
        launcher.pelotaforce(throughForce);
    }
    void Update()
    {
        GetComponent<Rigidbody>().AddForce(throughForce * transform.forward, ForceMode.Impulse);
    }
}