﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item_Trap_Script : MonoBehaviour
{
    public bool RecoverAmmo;
    public bool RecoverMissiles;
    public bool RecoverHealth;
    public int Healing;
    public bool DamagingEntity;
    public int DamageValue;
    public bool SelfDestroy;
    public bool Enemy;


    [SerializeField] AmmoManager ManagerAmmo;
    [SerializeField] LifeManager ManagerLife;

    private void OnCollisionEnter(Collision collision)
    {
        if (RecoverAmmo == true)
        {
            ManagerAmmo.RecoverBullet();
            SelfDestroy();
        }

        if (RecoverMissiles == true)
        {
            ManagerAmmo.RecoverMissile();
            SelfDestroy();
        }

        if(RecoverHealth == true)
        {
            ManagerLife.HealingReceived(Healing);
            SelfDestroy();
        }

        if (DamagingEntity == true)
        {
            ManagerLife.DamageReceivedV2(DamageValue);
        }

        if (Enemy == true)
        {
            ManagerLife.PlayerBulletDamage(DamageValue);
        }

        void SelfDestroy()
        {
            Destroy(this.gameObject);
        }
    }
}
