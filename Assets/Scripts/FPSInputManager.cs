﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSInputManager : MonoBehaviour
{
    //[SerializeField] Player player;
    [SerializeField] LookRotation lookRotation;
    [SerializeField] private float sensitivity = 3.0f;
    private Vector2 mouseAxis;
    private Vector2 inputAxis;

    private void Update()
    {
        // Modificar la camara del player

        // Rotación de la cámara
        mouseAxis = Vector2.zero;
        mouseAxis.x = Input.GetAxis("Mouse X") * sensitivity;
        mouseAxis.y = Input.GetAxis("Mouse Y") * sensitivity;
        lookRotation.SetRotation(mouseAxis);
        Debug.Log("Raton = " + mouseAxis.ToString());
    }
}
