﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    [SerializeField] Text MaxLife;
    [SerializeField] Text CurrentLife;
    [SerializeField] Text MaxBullets;
    [SerializeField] Text CurrentBullets;
    [SerializeField] Text MaxMissles;
    [SerializeField] Text CurrentMissles;
    [SerializeField] Text MaxEnemies;
    [SerializeField] Text EnemyCounter;

    private int MaxBulletValue;
    private int BulletValue;
    private int MaxMissileValue;
    private int MissileValue;
    private int MaxLifeValue;
    private int LifeValue;
    public int MaxEnemyValue;
    private int EnemyValue;

    // Start is called before the first frame update

    void Start()
    {
        MaxLife.text = "000";
        CurrentLife.text = "000";
        MaxBullets.text = "000";
        CurrentBullets.text = "000";
        MaxMissles.text = "00";
        CurrentMissles.text = "00";
        MaxEnemies.text = "00";
        EnemyCounter.text = "00";
    }

    // Update is called once per frame
    void Update()
    {
        if (EnemyValue >= MaxEnemyValue)
        {
            EnemyValue = MaxEnemyValue;
            SceneManager.LoadScene("Win");
        }
        GetMaxValues();
        UpdateValues();
    }

    void GetMaxValues()
    {
        MaxBullets.text = MaxBulletValue.ToString("000");
        MaxMissles.text = MaxMissileValue.ToString("00");
        MaxLife.text = MaxLifeValue.ToString("000");
        MaxEnemies.text = MaxEnemyValue.ToString("00");
    }

    void UpdateValues()
    {
        Debug.Log("Updated Ammo Values");
        CurrentBullets.text = BulletValue.ToString("000");
        CurrentMissles.text = MissileValue.ToString("00");
        CurrentLife.text = LifeValue.ToString("000");
        EnemyCounter.text = EnemyValue.ToString("00");
    }

    public void SetMaxBullets(int maxBulletVal)
    {
        Debug.Log("Received Max Bullet Count");
        MaxBulletValue = maxBulletVal;
    }
    public void SetMaxMissiles(int maxMissileVal)
    {
        Debug.Log("Received Max Bullet Count");
        MaxMissileValue = maxMissileVal;
    }
    public void SetMaxLife(int maxLifeVal)
    {
        Debug.Log("Received Max Life Count");
        MaxLifeValue = maxLifeVal;
    }
    public void SetBullets(int bulletVal)
    {
        BulletValue = bulletVal;
    }
    public void SetMissiles(int missilesVal)
    {
        MissileValue = missilesVal;
    }
    public void SetLife(int lifeVal)
    {
        Debug.Log("Updated Life Value");
        LifeValue = lifeVal;
    }

    public void EnemyKilled()
    {
        EnemyValue += 1;
    }
}
