﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class LifeManager : MonoBehaviour
{
    public int LifeValue;
    public int MaxLifeValue;
    private int DamageValue;
    public bool PlayerCharacter;
    public bool EnemyCharacter;
    public bool IsInvincible = false;
    public float InvincibleTime;
    private float InvTemp;
    public bool IsDead;
    public bool Vanguard;
    public bool GenericEnemy;

    public GameObject character;
    public GameObject PlayerBullet;
    private bool PBullet = false;
    public GameObject PlayerRocket;
    private bool PRocket = false;
    public GameObject EnemyBullet;
    private bool EBullet = false;

    [SerializeField] HUDManager ManagerHud;
    [SerializeField] VanguardManager ManagerVarduard;

    private void Start()
    {
        MaxLifeValue = LifeValue;
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerCharacter == true)
        {
            ManagerHud.SetMaxLife(MaxLifeValue);
            ManagerHud.SetLife(LifeValue);

            if (LifeValue >= MaxLifeValue)
            {
                LifeValue = MaxLifeValue;
            }

            if (LifeValue <= 0)
            {
                LifeValue = 0;
            }
        }

        /*
        if (IsInvincible == true)
        {
            Debug.Log("Invincibility active");
            if (InvTemp < InvincibleTime)
            {
                InvTemp += 1 * Time.deltaTime;
            }
            else
            {
                Debug.Log("Invincibility Expired");
                IsInvincible = false;
            }
        }
        */ 

        if (LifeValue <= 0)
        {
            Death();
        }
        /*
        if (IsDead == true)
        {
            Destroy(this.gameObject);
        }
       */
    }

    public void DamageRecieved(int Damage)
    {
        Debug.Log("Applying Damage");
        DamageValue = Damage;
    }

    public void DamageReceivedV2(int DamageV2)
    {
        Debug.Log("Applying Damage (V2)");
        if (PlayerCharacter == true)
        {
            LifeValue -= DamageV2;
        }
    }

    public void PlayerBulletDamage(int BulletDamage)
    {
        Debug.Log("Applying Bullet Damage (V2)");
        if (EnemyCharacter == true)
        {
            LifeValue -= BulletDamage;
        }
    }

    public void HealingReceived(int Healing)
    {
        Debug.Log("Restoring Health");
        LifeValue += Healing;
    }

    void ResetDamage()
    {
        Debug.Log("Resetting damage value");
        DamageValue = 0;
    }

    /*
    private void OnCollisionEnter(Collision collision)
    {
        if (EnemyCharacter == true)
        {
            if (collision.gameObject.tag == "PlayerBullet")
            {
                Debug.Log("Collision: PlayerBullet");
                PBullet = true;
                ResetDamage();
                LoseLife();
            }else if (collision.gameObject.tag == "PlayerRocket")
            {
                Debug.Log("Collision: PlayerRocket");
                PRocket = true;
                ResetDamage();
                LoseLife();
            }else if (collision.gameObject.tag == "EnemyBullet")
            {
                Debug.Log("Collision: EnemyBullet");
                EBullet = true;
                ResetDamage();
                LoseLife();
                IsInvincible = true;
            }
        }
    }

    void LoseLife()
    {
        if (PlayerCharacter == true){
            if (IsDead == false)
            {
                if (PBullet == true || EBullet == true)
                {
                    Debug.Log("Bullet: Damage Dealt");
                    LifeValue -= DamageValue;
                }else if (PRocket == true)
                {
                    Debug.Log("Rocket: Damage Dealt");
                    LifeValue -= (DamageValue * 3);
                }
                
                if (LifeValue <= 0)
                {
                    Debug.Log("Player Died");
                    IsDead = true;
                }
            }
        }else if (EnemyCharacter == true)
        {
            Debug.Log("Emeny received damage");
            DamageValue -= LifeValue;
        }
    }
    */

    void Death()
    {
        if (IsDead == false)
        {
            Debug.Log("Activating Death Sequence");
            ManagerHud.EnemyKilled();
        

            if (GenericEnemy == true)
            {
                character.SetActive(false);
            }
            if (Vanguard == true)
            {
                Debug.Log("Vanguard dying");
                ManagerVarduard.SetDead();
            }
            if (PlayerCharacter == true)
            {
                SceneManager.LoadScene("GameOver");
            }
            IsDead = true;
        }
       
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "PlayerBullet")
        {
            if (EnemyCharacter == true)
            {
                Debug.Log("Player Bullet collided with Enemy");
                LifeValue -= 1;
            }
        }

        if (other.tag == "PlayerRocket")
        {
            if (EnemyCharacter == true)
            {
                Debug.Log("Player Bullet collided with Enemy");
                LifeValue -= 5;
            }
        }

        if (other.tag == "EnemyBullet")
        {
            if (PlayerCharacter == true)
            {
                Debug.Log("Enemy Bullet collided with Player");
                LifeValue -= 10;
            }
        }
    }
}
