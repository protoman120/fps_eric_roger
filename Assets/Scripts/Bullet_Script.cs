﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Script : MonoBehaviour
{
    public bool Player_Bullet;
    public bool Enemy_Bullet;
    public int Damage_Value;

    private LifeManager ManagerLife;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Enemy")
        {
            if (Player_Bullet == true)
            {
                Debug.Log("Player Bullet collided with Enemy");
                ManagerLife.PlayerBulletDamage(Damage_Value);
            }
        }

        if (other.tag == "Player")
        {
            if (Enemy_Bullet == true)
            {
                Debug.Log("Enemy Bullet collided with Player");
                ManagerLife.DamageReceivedV2(Damage_Value);
            }
        }
    }
}
