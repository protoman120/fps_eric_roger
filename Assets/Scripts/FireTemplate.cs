﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FireTemplate : MonoBehaviour {
    public abstract void Fire();
    public abstract void Fire2();
} 
