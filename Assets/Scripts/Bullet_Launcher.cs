﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Launcher : FireTemplate
{
    public float throughForce;
    public bool launched = false;

    public override void Fire()
    {
        launched = true;
    }

    public override void Fire2()
    {
        launched = true;
    }


    private void FixedUpdate()
    {
        if (launched)
        {
            GetComponent<Rigidbody>().AddForce(throughForce * transform.forward, ForceMode.Force);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(this.gameObject);
    }

}
