﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    
    [SerializeField] GameObject MusicBackground;

    void Awake()
    {
        if (PlayerPrefs.GetInt("music", 1) == 1)
        {
            MusicBackground.SetActive(true);
        }
        else
        {
            MusicBackground.SetActive(false);
        }
    }
    

    public void PulsaPlay(){
        Debug.LogError("He pulsado Play");

        SceneManager.LoadScene("Level1");
    }

    public void PulsaOptions(){
        Debug.LogError("He pulsado Options");

        //StartCoroutine(DisableAnimation());
        PlayerPrefs.SetInt("animacion", 0);

        SceneManager.LoadScene("Options");
    }

    public void PulsaCredits()
    {
        Debug.LogError("He pulsado Credits");

       // StartCoroutine(DisableAnimation());

        SceneManager.LoadScene("Credits");
    }

    public void PulsaExit(){
        Application.Quit();
    }

}
