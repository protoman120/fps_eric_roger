﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionsManager : MonoBehaviour
{
    private int music = 1;
    private int sound = 1;


    [SerializeField] GameObject MusicON;
    [SerializeField] GameObject MusicOFF;
    [SerializeField] GameObject SoundON;
    [SerializeField] GameObject SoundOFF;

    [SerializeField] GameObject MusicBackground;
    //[SerializeField] GameObject Sound;

    public void PulsaBacktoMenu()
    {
        Debug.Log("He pulsado BacktoMenu");

        SceneManager.LoadScene("MainMenu");

    }

    public void PulsaMusic()
    {
        Debug.Log("He pulsado music");

        if (music == 1)
        {
            music = 0;
            PlayerPrefs.SetInt("music", 0);
            MusicBackground.SetActive(false);
        }
        else
        {
            music = 1;
            PlayerPrefs.SetInt("music", 1);
            MusicBackground.SetActive(true);
        }

    }

    public void PulsaSound()
    {
        Debug.Log("He pulsado sonido");

        if (sound == 1)
        {
            sound = 0;
            PlayerPrefs.SetInt("sound", 0);
            //Sound.SetActive(false);
        }
        else
        {
            sound = 1;
            PlayerPrefs.SetInt("sound", 1);
            //Sound.SetActive(true);
        }
    }



    void Awake()
    {

        if (PlayerPrefs.GetInt("music", 1) == 1)
        {
            MusicON.SetActive(true);
            MusicOFF.SetActive(false);

            MusicBackground.SetActive(true);
        }
        else
        {
            MusicON.SetActive(false);
            MusicOFF.SetActive(true);

            MusicBackground.SetActive(false);
        }


        if (PlayerPrefs.GetInt("sound", 1) == 1)
        {
            SoundON.SetActive(true);
            SoundOFF.SetActive(false);
        }
        else
        {
            SoundON.SetActive(false);
            SoundOFF.SetActive(true);
        }

    }


     void Update()
     {
         if (music == 1)
         {
            MusicON.SetActive(true);
            MusicOFF.SetActive(false);
        }
        else
        {
            MusicON.SetActive(false);
            MusicOFF.SetActive(true);
        }


        if (sound == 1)
        {
            SoundON.SetActive(true);
            SoundOFF.SetActive(false);
        }
        else
        {
            SoundON.SetActive(false);
            SoundOFF.SetActive(true);
        }
            
    }
}
