﻿using System.Collections;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class VanguardManager : MonoBehaviour
{
    public enum State { Idle, Patrol, Chase, Attack, Dead};
    public State state;

    private NavMeshAgent agent;
    private Animator anim;
    [SerializeField] SoundPlayer sound;

    [Header("Vanguard properties")]
    public int life = 5;
    public bool IsDead;
    public Collider collider01;
    public Collider collider02;

    [Header("Target Detection")]
    public float radius = 15.0f;
    public float idleRadius = 15.0f;
    public float chaseRadius = 15.0f;
    public LayerMask targetMask;
    public bool targetDetected = false;
    private Transform targetTransform;

    [Header("Patrol path")]
    public bool stopAtEachNode = true;
    public float timeStopped = 2.0f;
    private float timeCounter = 0.0f;
    public Transform[] pathNodes;
    private int currentNode = 0;
    private bool nearNode = false;

    [Header("Attack properties")]
    public int damageValue;
    public float attackDistance = 3.0f;
    public bool attack = false;
    public float attackStopped = 10.0f;
    public float waittime = 0.5f;
    private float attackCounter;

    public LifeManager managerlife;

    private int randomattack;
    // Use this for initialization
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponentInChildren<Animator>();

        nearNode = true;
        SetIdle();

        attackCounter = waittime;
    }

    // Update is called once per frame
    void Update()
    {
        switch (state)
        {
            case State.Idle:
                Idle();
                break;
            case State.Patrol:
                Patrol();
                break;
            case State.Chase:
                Chase();
                break;
            case State.Attack:
                Attack();
                break;
            case State.Dead:
                Dead();
                break;
            default:
                break;
        }
    }
    private void FixedUpdate()
    {
        targetDetected = false;
        Collider[] cols = Physics.OverlapSphere(this.transform.position, radius, targetMask);
        if (cols.Length != 0)
        {
            targetDetected = true;
            targetTransform = cols[0].transform;
        }
    }

    void Idle()
    {
        if (targetDetected)
        {
            SetChase();
            return;
        }

        if (timeCounter >= timeStopped)
        {
            if (nearNode) GoNearNode();
            else GoNextNode();
            SetPatrol();
        }
        else timeCounter += Time.deltaTime;
    }
    void Patrol()
    {
        if (targetDetected)
        {
            SetChase();
            return;
        }

        if (agent.remainingDistance <= 0.3f)
        {
            if (stopAtEachNode) SetIdle();
            else GoNextNode();
        }
    }
    void Chase()
    {
        if (!targetDetected)
        {
            nearNode = true;
            SetIdle();
            return;
        }
        agent.SetDestination(targetTransform.position);

        //Debug.LogError("agent distance: " + agent.remainingDistance);
        //Debug.LogError("distance: " + Vector3.Distance(transform.position, targetTransform.position));
        if (Vector3.Distance(transform.position, targetTransform.position) <= attackDistance)
        {
            SetAttack();
        }


    }

    private void Attack()
    {
        Debug.Log("Attack");
        if (attackCounter >= waittime)
        {
            anim.SetBool("Idle", false);
            randomattack = Random.Range(1, 3);
            Debug.Log("randomattack: " + randomattack);

            if (randomattack == 1)
            { 
                anim.SetBool("attack1", true);
                Debug.Log("Animacion attack1");
            }
            else if (randomattack == 2)
            {
                anim.SetBool("attack2", true);
                Debug.Log("Animacion attack2");
            }
            else
            {
                anim.SetBool("attack3", true);
                Debug.Log("Animacion attack3");
            }

            managerlife.DamageReceivedV2(damageValue);

            anim.SetBool("Idle", true);
            anim.SetBool("walking", false);
            attackCounter = 0;
        }
        else
        {
            attackCounter += Time.deltaTime;
        }

        if (Vector3.Distance(transform.position, targetTransform.position) > attackDistance)
        {
            SetChase();
        }


    }

    void Dead() 
    {
        if (IsDead == false)
        {
            Debug.Log("Starting animation");
            agent.isStopped = true;
            //sound.Play(0, 1);
            anim.SetTrigger("death");
            collider01.enabled = false;
            collider02.enabled = false;
            IsDead = true;
        }
        
    }

    void SetIdle()
    {
        agent.isStopped = true;
        anim.SetBool("walking", false);
        radius = idleRadius;
        timeCounter = 0;

        int random = Random.Range(0, 4);
        //sound.Play(0, 1);

        state = State.Idle;
    }
    void SetPatrol()
    {
        agent.isStopped = false;
        agent.stoppingDistance = 0;
        radius = idleRadius;
        anim.SetBool("walking", true);

        state = State.Patrol;
    }
    void SetChase()
    {
        Debug.Log("Chase");
        agent.isStopped = false;
        agent.SetDestination(targetTransform.position);
        agent.stoppingDistance = 2.0f;
        anim.SetBool("walking", true);
        radius = chaseRadius;

        state = State.Chase;
    }
    void SetAttack()
    {
        Debug.Log("attack");
        attack = true;
        //sound.Play(0, 1);
        agent.isStopped = true;
        

        state = State.Attack;

    }
    public void SetDead()
    {
        state = State.Dead;
    }

    void GoNextNode()
    {
        currentNode++;
        if (currentNode >= pathNodes.Length) currentNode = 0;

        agent.SetDestination(pathNodes[currentNode].position);
    }
    void GoNearNode()
    {
        nearNode = false;
        float minDistance = Mathf.Infinity;
        for (int i = 0; i < pathNodes.Length; i++)
        {
            if (Vector3.Distance(transform.position, pathNodes[i].position) < minDistance)
            {
                minDistance = Vector3.Distance(transform.position, pathNodes[i].position);
                currentNode = i;
            }
        }
        agent.SetDestination(pathNodes[currentNode].position);
    }

    private void OnDrawGizmos()
    {
        Color color = Color.blue;
        if (targetDetected) color = Color.red;
        color.a = 0.1f;
        Gizmos.color = color;
        Gizmos.DrawSphere(this.transform.position, radius);
    }

}
