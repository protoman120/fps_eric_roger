﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoManager : MonoBehaviour
{
    public int BulletAmmo;
    private int MaxBulletAmmo;
    public int MissileAmmo;
    private int MaxMissileAmmo;
    public int MissleBoxRecovery;
    public int BulletBoxRecovery;
    [SerializeField] FPSInputManager2 ImputManager;
    [SerializeField] HUDManager ManagerHud;



    void Start()
    {
        MaxBulletAmmo = BulletAmmo;
        MaxMissileAmmo = MissileAmmo;
    }

    // Update is called once per frame
    void Update()
    {
        ManagerHud.SetMaxBullets(MaxBulletAmmo);
        ManagerHud.SetMaxMissiles(MaxMissileAmmo);
        BulletManager();
        MissileManager();
        ManagerHud.SetBullets(BulletAmmo);
        ManagerHud.SetMissiles(MissileAmmo);
    }

    public void BulletUsed()
    {
        if (BulletAmmo > 0)
        {
            Debug.Log("Bullet Used");
            BulletAmmo -= 1;
            ImputManager.BulletShot();
        }
    }

    public void MissileUsed()
    {
        if (MissileAmmo > 0)
        {
            Debug.Log("Missle Used");
            MissileAmmo -= 1;
            ImputManager.MissileShot();
        }
    }

    void BulletManager()
    {
        if (BulletAmmo < 0)
        {
            Debug.Log("No Bullets Left");
            BulletAmmo = 0;
        } else if (BulletAmmo > MaxBulletAmmo)
        {
            Debug.Log("Max Bullets");
            BulletAmmo = MaxBulletAmmo;
        }
    }

    void MissileManager()
    {
        if (MissileAmmo < 0)
        {
            Debug.Log("No Missiles Left");
            MissileAmmo = 0;
        } else if (MissileAmmo > MaxMissileAmmo)
        {
            Debug.Log("Max Missiles");
            MissileAmmo = MaxMissileAmmo;
        }
    }

    public void RecoverBullet()
    {
        Debug.Log("Extra Bullets Aquired");
        BulletAmmo += BulletBoxRecovery;
    }

    public void RecoverMissile()
    {
        Debug.Log("Extra Missiles Aquired");
        MissileAmmo += MissleBoxRecovery;
    }

}
